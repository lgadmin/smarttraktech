      <?php wp_nav_menu( array(
        'theme_location'    => 'company-menu',
        'depth'             => 1,
        'container'         => 'div',
        'container_class'   => 'container',
        // 'container_id'      => 'main-navbar',
        'menu_class'        => 'company-menu',
        'fallback_cb'       => 'wp_bootstrap_navwalker::fallback',
        'walker'            => new wp_bootstrap_navwalker())
      ); ?>
