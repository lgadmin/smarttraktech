<section class="panel-group panel-service" id="accordion" role="tablist" aria-multiselectable="true">
	<?php $mycount = 0; ?>
	<?php if( have_rows('accordions') ): ?>
		<?php while( have_rows('accordions') ): the_row(); ?>
			<div class="panel panel-default lg-accordion">
				
				<div class="panel-heading" role="tab" id="heading-<?php echo $mycount; ?>">
					<h4 class="panel-title">
						<a>
							<?php the_sub_field('title'); ?><small><?php the_sub_field('accordion_sub_title'); ?></small>
						</a> 
					</h4>
				</div>
				
				<div id="collapse-<?php echo $mycount; ?>" class="panel-collapse collapse <?php // if($mycount == 0){echo "in";} ?>" role="tabpanel" aria-labelledby="heading-<?php echo $mycount; ?>">
					<div class="panel-body">
						<p><?php the_sub_field('accordion_content'); ?></p>
					</div>
				</div>
			</div>
			<?php $mycount = $mycount + 1; ?>
		<?php endwhile; ?>
	<?php endif; ?>
</section>