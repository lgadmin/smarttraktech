<div id="carousel-example-generic" class="carousel slide" data-ride="carousel" data-interval="6000">

 <!-- Indicators -->
  <ol class="carousel-indicators">
    <li data-target="#carousel-example-generic" data-slide-to="0" class="active"></li>
    <li data-target="#carousel-example-generic" data-slide-to="1"></li>
    <li data-target="#carousel-example-generic" data-slide-to="2"></li>
    <li data-target="#carousel-example-generic" data-slide-to="3"></li>
  </ol>


	<div class="carousel-inner" role="listbox">
		

		<!-- slider 1 -->
		<div class="item active">

		<div class="row">
			<div class="col-md-12 slide1">

			<!--black box-->
			<div class="row">
				<div class="col-xs-12 col-md-6 box-screen"> </div>
			</div>

			<div class="col-md-6 hero-text">

				<div class="row">
					<span class="spot">Leader&nbsp;in Loss&nbsp;Prevention</span> <br>
					<span class="sm-spot">Increased security and visibility for intermodal cargo in transit.</span> <br>
					<a href="/seal-trak/" class="btn btn-primary"></span>Discover How</a>
				</div>

			</div>

				</div>
			</div>
		</div>







		<!-- slider 2 -->
		<div class="item">

		<div class="row">
			<div class="col-md-12 slide2">

			<!--black box-->
			<div class="row">
				<div class="col-xs-12 col-md-6 box-screen"> </div>
			</div>

			<div class="col-md-6 hero-text">

				<div class="row">
					<span class="spot">Leader&nbsp;in Loss&nbsp;Prevention</span>
					<br>
					<span class="sm-spot">Increased security and visibility for intermodal cargo in transit.</span>
					<br>
					<a href="/seal-trak/" class="btn btn-primary"></span>Learn More</a>
				</div>

				</div>

				</div>
			</div>
		</div>


		<!-- slider 3 -->
		<div class="item">

		<div class="row">
			<div class="col-md-12 slide3">

			<!--black box-->
			<div class="row">
				<div class="col-xs-12 col-md-6 box-screen"> </div>
			</div>

			<div class="col-md-6 hero-text">

				<div class="row">
					<span class="spot">Leader&nbsp;in Loss&nbsp;Prevention</span>
					<br>
					<span class="sm-spot">Increased security and visibility for intermodal cargo in transit.</span>
					<br>
					<a href="/seal-trak/" class="btn btn-primary"></span>Learn More</a>
				</div>

				</div>

				</div>
			</div>
		</div>

		<!-- slider 4 -->
		<div class="item">

		<div class="row">
		<div class="col-md-12 slide4">

		<!--black box-->
		<div class="row">
		<div class="col-xs-12 col-md-6 box-screen"> </div>
		</div>

		<div class="col-md-6 hero-text">

		<div class="row">
			<span class="spot">Leader&nbsp;in Loss&nbsp;Prevention</span>
			<br>
			<span class="sm-spot">Increased security and visibility for intermodal cargo in transit.</span>
			<br>
			<a href="/seal-trak/" class="btn btn-primary"></span>Learn More</a>
		</div>

		</div>

		</div>
		</div>
		</div>

	</div>
</div>
