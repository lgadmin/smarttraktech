<?php if ( has_post_thumbnail()) : ?>
	<div class="featured-image">
		<div class="title-cont">
			<span class="featured-title"><?php echo get_the_title(); ?></span>
		</div>
		<div class="img-cont">
			<img src="<?php echo get_stylesheet_directory_uri() . '/images/banner-header.jpg'; ?>" alt="">
		</div>
	</div>
<?php else : ?>
	<div class="featured-image">
		<div class="title-cont">
		<?php if (is_post_type_archive('stt-events')): ?>
			<span class="featured-title">Events</span>
		<?php elseif (is_post_type_archive('stt-news')): ?>
			<span class="featured-title">Articles</span>
		<?php else : ?>
			<span class="featured-title"><?php echo get_the_title(); ?></span>
		<?php endif; ?>
		</div>
		<div class="img-cont">
			<img src="<?php echo get_stylesheet_directory_uri() . '/images/banner-header.jpg'; ?>" alt="">
		</div>
	</div>
<?php endif ?>