	<?php 
		$args = array( 'post_type' => 'stt-events', 'posts_per_page' => 4 );
		$loop = new WP_Query( $args );
	?>
	<div class="column-events">
		<?php while ( $loop->have_posts() ) : $loop->the_post()  ?>
			<a href="/stt-events/">
			<p><b><?php the_title(); ?></b></p>
			<div class="entry-content"> <?php the_excerpt(); ?> </div>
			</a>
		<?php endwhile; ?>
	</div>
