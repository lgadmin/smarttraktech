	<?php 
		$args = array( 'post_type' => 'stt-news', 'posts_per_page' => 3 );
		$loop = new WP_Query( $args );
	?>
	<div class="column-news">
		<?php while ( $loop->have_posts() ) : $loop->the_post()  ?>
			<a href="/stt-news/">
			<p><b><?php the_title(); ?></b></p>
			<div class="entry-content"> <?php the_excerpt(); ?> </div>
			</a>
		<?php endwhile; ?>
	</div>
