<div class="cta-events container-fluid">
	<div class="container">
		<div class="row">
			
			<div class="col-xs-12 col-md-4 col-lg-4 box">
				<h3>Corporate Overview</h3>
				<p>Smart Trak Technologies (STT) is a developer of supply chain technologies to ensure the integrity and security of cargo assets.</p>

				<p>We built our company with the primary focus of real-time security in mind. Our products offer sophisticated insights to streamline the entire supply chain process.</p>
			</div>

			<div class="col-xs-12 col-sm-6 col-md-4 col-lg-4 box" >
				<h3>In the News</h3>
				<p class="center">Keep up to date the recent news.</p>
				<?php get_template_part("/inc/cta-news-feed"); ?>
			</div>

			<div class="col-xs-12 col-sm-6 col-md-4 col-lg-4 box">
				<h3>Upcoming Events</h3>
				<p class="center">Keep up to date with upcoming events.</p>
				<?php get_template_part("/inc/cta-events-feed"); ?>
			</div>

		</div>
	</div>
</div>