<div id="carousel-example-generic" class="carousel slide" data-ride="carousel" data-interval="6000">

  <!-- Indicators -->
  <ol class="carousel-indicators">
    <li data-target="#carousel-example-generic" data-slide-to="0" class="active"></li>
    <li data-target="#carousel-example-generic" data-slide-to="1"></li>
    <li data-target="#carousel-example-generic" data-slide-to="2"></li>
    <li data-target="#carousel-example-generic" data-slide-to="3"></li>
  </ol>

  <!-- Copy -->
  <div class="slide-content">
    
    <!--black box-->
      <div class="box-container">
        <div class="box-screen"></div> 
        
        <div class="hero-text">
          <span class="spot">Leaders&nbsp;in Loss&nbsp;Prevention</span> <br>
          <span class="sm-spot">Increased security and visibility for intermodal cargo&nbsp;in&nbsp;transit.</span> <br>
          <a href="/seal-trak/" class="btn btn-primary"></span>Discover How</a>
        </div>
      </div>


  </div>

  <!-- Wrapper for slides -->
  <div class="carousel-inner" role="listbox">
    
    <!-- Slide 1 -->
    <div class="item active">
      <div class="img-cont"><img src="<?php echo get_stylesheet_directory_uri() . '/images/hero-truck-hero.jpg'; ?>" alt=""></div>
    </div>
 
    <!-- Slide 2 -->
    <div class="item">
      <div class="img-cont"><img src="<?php echo get_stylesheet_directory_uri() . '/images/hero-container-hero.jpg'; ?>" alt=""></div>
    </div>

    <!-- Slide 3 -->
    <div class="item">
      <div class="img-cont"><img src="<?php echo get_stylesheet_directory_uri() . '/images/sat-vertical.jpg'; ?>" alt=""></div>
    </div>

    <!-- Slide 4 -->
    <div class="item">
      <div class="img-cont"><img src="<?php echo get_stylesheet_directory_uri() . '/images/hero-train-hero.jpg'; ?>" alt=""></div>
    </div>

  </div>

</div>