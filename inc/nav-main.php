<nav class="navbar navbar-default">
  <div class="container-fluid">
    <div class="navbar-header">
     
        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#main-navbar">
          <span class="sr-only">Toggle navigation</span> Menu <i class="fa fa-bars" aria-hidden="true"></i>
        </button>
        
        <?php the_custom_logo(); ?>

      </div>

      <?php wp_nav_menu( array(
    		'theme_location'    => 'primary-menu',
    		'depth'             => 2,
    		'container'         => 'div',
    		'container_class'   => 'collapse navbar-collapse',
    		'container_id'      => 'main-navbar',
    		'menu_class'        => 'nav navbar-nav navbar-right',
    		'fallback_cb'       => 'wp_bootstrap_navwalker::fallback',
    		'walker'            => new wp_bootstrap_navwalker())
      ); ?>

  </div>
</nav>
  