<div class="cta-mission container-fluid ">
	<div class="container">
		<div class="row">
			
			<div class="col-xs-12">
				<div>
					<h2 class="mission-copy"><span class="white"><span class="gold">Smart Trak Technologies </span>is a worldwide leader in loss prevention, securing goods in transit or storage through a proprietary system of portable, cost effective, state of the art tracking devices.</span></h2>

				</div>
			</div>

		</div>
	</div>
</div>
