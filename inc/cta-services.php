<div class="cta-services container-fluid ">
	<div class="container">
		<div class="row">
			
			<div class="col-xs-12 col-sm-4">
				<div class="well">
					<div class="cta-img">
						<img src="<?php echo get_stylesheet_directory_uri() . '/images/secure-icon.png'; ?>"  alt="secure icon" class="img-responsive">
					</div>

						<h1>Secure</h1>
							<p>Portable GPS-enabled<br>tracking devices.&nbsp;&nbsp;&nbsp;</p>
							<a href="/secure/" class="btn btn-default">Learn More</a>
				</div>
			</div>

			<div class="col-xs-12 col-sm-4">
				<div class="well">
					<div class="cta-img">
						<img src="<?php echo get_stylesheet_directory_uri() . '/images/track-icon.png'; ?>"  alt="track icon" class="img-responsive">
					</div>
						<h1>Track</h1>
							<p>Advanced wireless and<br>precision monitoring software.</p>
							<a href="/track/" class="btn btn-default">Learn More</a>
				</div>
			</div>

			<div class="col-xs-12  col-sm-4">
				<div class="well">
					<div class="cta-img">
					<img src="<?php echo get_stylesheet_directory_uri() . '/images/notify-icon.png'; ?>"  alt="notify icon" class="img-responsive">
					</div>
						<h1>Notify</h1>
							<p>Integrated system alerts<br>for real-time visibility.</p>
							<a href="/notify/" class="btn btn-default">Learn More</a>
				</div>
			</div>

		</div>
	</div>
</div>
