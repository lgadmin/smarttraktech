<!-- Default Address Stuff -->
	<address itemscope="" itemtype="http://schema.org/LocalBusiness">
		
		<a href="/" class="footer-logo"><img src="<?php echo get_stylesheet_directory_uri() . '/images/CFSD-footer-logo.png'; ?>" alt="Cross Fit South Delta Logo"></a><br>

		<span class="card-map-marker" itemprop="address" itemscope="" itemtype="http://schema.org/PostalAddress">
			<span itemprop="streetAddress">7198 Vantage Way, Unit&nbsp;117</span><br>
			<span itemprop="addressLocality">Delta</span>, <span itemprop="addressRegion">BC</span>&nbsp;<span itemprop="postalCode">V4G&nbsp;1k7</span><br>
		</span>
		
		<br>
		<span class="card-map-phone" itemprop="telephone"><a href="tel:+7789087150">(778) 908-7150</a></span><br>
		
		<br>
		<a class="card-map-email" href="mailto:info@crossfisouthdelta.ca">info@crossfisouthdelta.ca</a>

	</address>
 
