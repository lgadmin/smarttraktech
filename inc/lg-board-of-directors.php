<section class="panel-group panel-service" id="board-of-directors" role="tablist" aria-multiselectable="true">
	<?php $mycount = 0; ?>
	<?php if( have_rows('board_of_directors') ): ?>
		<?php while( have_rows('board_of_directors') ): the_row(); ?>
			<div class="panel panel-default lg-accordion">
				<div class="panel-heading" role="tab" id="heading-<?php echo $mycount; ?>">
					<h4 class="panel-title">
						<a role="button" data-toggle="collapse" data-parent="#board-of-directors" aria-expanded="true" aria-controls="collapse-<?php echo $mycount; ?>">
							<?php the_sub_field('title'); ?>
							<br> <small><?php the_sub_field('sub-title'); ?></small>
						</a> 
					</h4>
				</div>
			</div>
			<?php $mycount = $mycount + 1; ?>
		<?php endwhile; ?>
	<?php endif; ?>
</section>