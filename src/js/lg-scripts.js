jQuery(document).ready(function(){
	jQuery('.collapse').on('shown.bs.collapse', function(){
		jQuery(this).parent().find(".fa-caret-down").removeClass("fa-caret-down").addClass("fa-caret-up");
		}).on('hidden.bs.collapse', function(){
		jQuery(this).parent().find(".fa-caret-up").removeClass("fa-caret-up").addClass("fa-caret-down");
	});
});

// client wanted the title removed from the main menu
jQuery(document).ready(function($) {
    $("#menu-main a").removeAttr("title");
    $('.site-content').css('marginBottom', $('.footer-wrapper').outerHeight());

    $('.scroll-top').on('click', function(){
        $('html, body').stop().animate({scrollTop : 0},800);
    });

    //home page slider fadein effect
    if($('#carousel-example-generic')[0] && $(window).width() > 768){
    	var offsetTop = $('#carousel-example-generic').offset().top - $('#masthead').outerHeight();
    	var elementHeight = $('#carousel-example-generic').outerHeight();
    	var breakpoint = offsetTop + elementHeight / 2.00;
    	var previousScrollTop = 0;

    	$(window).scroll(function(){
    		var currentScrollTop = $(window).scrollTop();
			//calculate percentage
			var onBlockScrollTop = currentScrollTop - offsetTop;
			var opacity = (elementHeight - onBlockScrollTop) / elementHeight;
			$('#carousel-example-generic .hero-text').css('opacity', opacity * opacity);
			$('#carousel-example-generic .carousel-inner, #carousel-example-generic .slide-content').css({'transform' : 'translateY(' + (onBlockScrollTop / elementHeight) * 300 + 'px)'});
	    });
    }

    //Footer effect in desktop only
    $(window).scroll(function(){
        if($(window).width() > 768){
            $('.site-content').css('marginBottom', $('.footer-wrapper').outerHeight());
        }else{
            $('.site-content').css('marginBottom', 0);
        }
		
    });

    //Technology
    $(window).resize(function(){
        technologyBoxResolveHeight();
    });

    technologyBoxResolveHeight();

    $('.single-tech').on('mouseover',function(){
        $(this).find('p').stop().slideDown();
    });

    $('.single-tech').on('mouseleave',function(){
        $(this).find('p').stop().slideUp();
    });

    
    function technologyBoxResolveHeight(){
        $('.single-tech').each(function(){
            console.log($(this));
            $(this).css('height', $(this).width());
        });
    }
    //End Technology


    //stt news read more
    $('.stt-news-read-more').on('click', function(){
        $('.stt-news').find('div.hide').removeClass('hide');
        $(this).fadeOut();
    });

    //end
});