<?php
/**
 * The template for displaying all pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package smarttraktech
 */

get_header(); ?>

<?php get_template_part("/inc/featured-image"); ?>


<div class="container pt-lg pb-lg">
	<div class="row">
		<div class="col-xs-12">
		
				<div id="primary" class="content-area">
					
					<div class="executive-management-team-wrap">
						<div class="executive-team">
							<h2>Executive Team</h2><br>
							<?php  get_template_part("/inc/lg-accordion"); ?>
						</div>

						<!--<div class="board-directors">
							<h2>Board of Directors</h2><br>
							<?php  get_template_part("/inc/lg-board-of-directors"); ?>						
						</div>-->
					</div>

				</div><!-- #primary -->
		</div>

	</div>
</div>

<?php get_footer(); ?>

