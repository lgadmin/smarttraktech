<?php
/**
 ** Template Name: Fullwidth Template
 */
?>

<?php get_header(); ?>

<?php get_template_part("/inc/featured-image"); ?>

<div class="container pt-lg pb-lg">
	<div class="row">
		<div class="col-xs-12 col-md-8">
			
				<div id="primary" class="content-area">
					<main id="main" class="site-main">
						<div class="stt-news">
							<h1>News</h1>
							<?php 
							$item_to_show = 3;
							$news = array();
							$args = array(
				                'numberposts'	=> -1,
				                'post_type'		=> 'stt-news',
				            );

				            // put here your query args
				            $result = new WP_Query( $args );

				            // Loop
				            if ( $result->have_posts() ) :
				            	$index = 1;
				                while( $result->have_posts() ) : $result->the_post();
				            		if($index > $item_to_show){
				            			?>
											<div class="hide">
				            			<?php
				            		}
				                    get_template_part( 'template-parts/content', get_post_format() );
				                    if($index > $item_to_show){
				                    ?>
										</div>
				                    <?php
				                	}
				                    $index++;
				                endwhile;
				            endif; // End Loop

				            if($index > $item_to_show){
				            	?>

								<a class="btn btn-default stt-news-read-more">READ MORE</a>
				            	<?php
				            }
							?>
						</div>
	
					</main><!-- #main -->
				</div><!-- #primary -->
	
		</div>
		<div class="col-xs-12 col-md-4">
			<?php get_sidebar(); ?>	
		</div>
	</div>
</div>

<?php get_footer(); ?>

