<?php
/**
 * smarttraktech functions and definitions
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package smarttraktech
 */

if ( ! function_exists( 'smarttraktech_setup' ) ) :
	/**
	 * Sets up theme defaults and registers support for various WordPress features.
	 *
	 * Note that this function is hooked into the after_setup_theme hook, which
	 * runs before the init hook. The init hook is too late for some features, such
	 * as indicating support for post thumbnails.
	 */
	function smarttraktech_setup() {
		/*
		 * Make theme available for translation.
		 * Translations can be filed in the /languages/ directory.
		 * If you're building a theme based on smarttraktech, use a find and replace
		 * to change 'smarttraktech' to the name of your theme in all the template files.
		 */
		load_theme_textdomain( 'smarttraktech', get_template_directory() . '/languages' );

		// Add default posts and comments RSS feed links to head.
		add_theme_support( 'automatic-feed-links' );

		/*
		 * Let WordPress manage the document title.
		 * By adding theme support, we declare that this theme does not use a
		 * hard-coded <title> tag in the document head, and expect WordPress to
		 * provide it for us.
		 */
		add_theme_support( 'title-tag' );

		/*
		 * Enable support for Post Thumbnails on posts and pages.
		 *
		 * @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
		 */
		add_theme_support( 'post-thumbnails' );

		// Menus
		function stonealpha_menus() {
		  register_nav_menus(
		    array(
		      'primary-menu' => __( 'Primary' ),
		      'footer-menu' => __( 'Footer' ),
		      'utility-menu' => __( 'Utility' ),
		      'company-menu' => __( 'Company' ),
		    )
		  );
		}
		add_action( 'init', 'stonealpha_menus' );

		/*
		 * Switch default core markup for search form, comment form, and comments
		 * to output valid HTML5.
		 */
		add_theme_support( 'html5', array(
			'search-form',
			'comment-form',
			'comment-list',
			'gallery',
			'caption',
		) );

		// Set up the WordPress core custom background feature.
		add_theme_support( 'custom-background', apply_filters( 'smarttraktech_custom_background_args', array(
			'default-color' => 'ffffff',
			'default-image' => '',
		) ) );

		// Add theme support for selective refresh for widgets.
		add_theme_support( 'customize-selective-refresh-widgets' );

		/**
		 * Add support for core custom logo.
		 *
		 * @link https://codex.wordpress.org/Theme_Logo
		 */
		add_theme_support( 'custom-logo', array(
			'height'      => 250,
			'width'       => 250,
			'flex-width'  => true,
			'flex-height' => true,
		) );
	}
endif;
add_action( 'after_setup_theme', 'smarttraktech_setup' );

/**
 * Set the content width in pixels, based on the theme's design and stylesheet.
 *
 * Priority 0 to make it available to lower priority callbacks.
 *
 * @global int $content_width
 */
function smarttraktech_content_width() {
	$GLOBALS['content_width'] = apply_filters( 'smarttraktech_content_width', 640 );
}
add_action( 'after_setup_theme', 'smarttraktech_content_width', 0 );

/**
 * Register widget area.
 *
 * @link https://developer.wordpress.org/themes/functionality/sidebars/#registering-a-sidebar
 */
function smarttraktech_widgets_init() {
	register_sidebar( array(
		'name'          => esc_html__( 'Sidebar', 'smarttraktech' ),
		'id'            => 'sidebar-1',
		'description'   => esc_html__( 'Add widgets here.', 'smarttraktech' ),
		'before_widget' => '<section id="%1$s" class="widget %2$s">',
		'after_widget'  => '</section>',
		'before_title'  => '<h2 class="widget-title">',
		'after_title'   => '</h2>',
	) );
}
add_action( 'widgets_init', 'smarttraktech_widgets_init' );

/**
 * Enqueue scripts and styles.
 */
function smarttraktech_scripts() {
	wp_enqueue_style( 'smarttraktech-style', get_stylesheet_uri(), '1.2.6', true );

	// Fonts
	wp_enqueue_style( 'crossfitsouthdelta-fonts', 'https://fonts.googleapis.com/css?family=Oswald:200,300,400,500,600,700|Roboto:100,100i,300,300i,400,400i,500,500i,700,700i,900,900i', false );

	// Register Scripts
	wp_register_script( 'bootstrapjs', get_template_directory_uri() . "/js/bootstrap.min.js", array('jquery'), '3.3.8', true );
	wp_register_script( 'smarttraktech-skip-link-focus-fix', get_template_directory_uri() . '/js/skip-link-focus-fix.js', array(), '20151215', true );
	wp_register_script( 'lg-scripts', get_template_directory_uri() . "/js/lg-scripts.js", array('jquery'), '1.0.2', true );

	// Enqueue scripts. Only load what script(s) are needed. Conditional statement if wish.
	wp_enqueue_script( 'smarttraktech-skip-link-focus-fix' );
	wp_enqueue_script( 'bootstrapjs' );
	wp_enqueue_script( 'lg-scripts' );


	if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
		wp_enqueue_script( 'comment-reply' );
	}
}
add_action( 'wp_enqueue_scripts', 'smarttraktech_scripts' );

/**
 * Implement the Custom Header feature.
 */
require get_template_directory() . '/inc/custom-header.php';

/**
 * Custom template tags for this theme.
 */
require get_template_directory() . '/inc/template-tags.php';

/**
 * Functions which enhance the theme by hooking into WordPress.
 */
require get_template_directory() . '/inc/template-functions.php';

/**
 * Customizer additions.
 */
require get_template_directory() . '/inc/customizer.php';

/**
 * Load Jetpack compatibility file.
 */
if ( defined( 'JETPACK__VERSION' ) ) {
	require get_template_directory() . '/inc/jetpack.php';
}

// Register Custom Navigation Walker
require_once(get_template_directory() . '/inc/wp-bootstrap-navwalker.php');



// Custom Post Types -- News and Events
	function create_post_type() {
	  
	  // Events
	  register_post_type( 'stt-events',
	    array(
	        'labels' => array(
	        'name' => __( 'Events' ),
	        'singular_name' => __( 'Event' )
	      ),
	      'public' => true,
	      'has_archive' => false,
	      'taxonomies' => array('post_tag','category'),
	    )
	  );

	  // News
	  register_post_type( 'stt-news',
	    array(
	        'labels' => array(
	        'name' => __( 'News' ),
	        'singular_name' => __( 'News' )
	      ),
	      'public' => true,
	      'has_archive' => false,
	      'taxonomies' => array('post_tag','category'),
	    )
	  );

	}
	add_action( 'init', 'create_post_type' );

	// Limit Excerpt limit. Did this for the front page output
	function custom_excerpt_length( $length ) {
		return 20;
	}
	add_filter( 'excerpt_length', 'custom_excerpt_length', 999 );

