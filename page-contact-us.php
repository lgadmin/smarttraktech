    <?php
/**
 * The template for displaying all pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package smarttraktech
 */
?>

<?php get_header(); ?>

<?php get_template_part("/inc/featured-image"); ?>

<div class="container pt-lg pb-lg">
	<div class="row">
		
		<div class="col-xs-12">
			
				<div id="primary" class="content-area">
					<main id="main" class="site-main">
	
						<?php
						while ( have_posts() ) : the_post();
	
							get_template_part( 'template-parts/content', 'page' );
	
							// If comments are open or we have at least one comment, load up the comment template.
							if ( comments_open() || get_comments_number() ) :
								comments_template();
							endif;
	
						endwhile; // End of the loop.
						?>
	
<!-- office locations -->
<div class="row">
<hr>
	<div class="col-xs-12 col-sm-4 col-md-4 col-lg-4 box">
		<h4><i class="fa fa-map-marker" style="color:#f2d412;"></i> CANADA</h4>
		
			
			<address itemscope="" itemtype="http://schema.org/LocalBusiness">
				<span class="card-map-marker" itemprop="address" itemscope="" itemtype="http://schema.org/PostalAddress">
					<span itemprop="streetAddress">
					1201-11871 Suite F, Horseshoe Way<br>
					Richmond, BC V7A 5H5<br>
					</span>
				</span>

				<span class="card-map-phone" itemprop="telephone">
					<a href="tel:+17782276031">Tel: (778) 227-6031</a>
				</span><br>
			</address>
	</div>

	<div class="col-xs-12 col-sm-4 col-md-4 col-lg-4 box">
		<h4><i class="fa fa-map-marker" style="color:#f2d412;"></i>  USA</h4>
		<strong>Corporate Headquarters</strong><br>
			<address>
			<span class="card-map-marker">
			Suite 2200 – 75 Fourteenth Street<br>
			Atlanta, GA 30309<br>
			</span>

			<span class="card-map-phone" itemprop="telephone">
					<a href="tel:+17782276031">Tel: (778) 227-6031</a>
			</span><br>
			</address>
	</div>

	<div class="col-xs-12 col-sm-4 col-md-4 col-lg-4 box">
		<h4><i class="fa fa-map-marker" style="color:#f2d412;"></i> MEXICO</h4>
			<span class="small">
			Camino Real a San Lorenzo # 263<br>
			Barrio San Miguel, Iztapalapa<br>
			CP 09360, CDMX, Mexico<br>
			</span>
	</div>
</div>


<!-- end -->

					</main><!-- #main -->
				</div><!-- #primary -->
	
		</div>
		
	</div>
</div>

<?php get_footer(); ?>

