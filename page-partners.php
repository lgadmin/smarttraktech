    <?php
/**
 * The template for displaying all pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package smarttraktech
 */
?>

<?php get_header(); ?>

<?php get_template_part("/inc/featured-image"); ?>

<div class="container pt-lg pb-lg">
	<div class="row">
		
		<div class="col-xs-12">
			
				<div id="primary" class="content-area">
					<main id="main" class="site-main">
	
						<?php
						while ( have_posts() ) : the_post();
	
							get_template_part( 'template-parts/content', 'page' );
	
							// If comments are open or we have at least one comment, load up the comment template.
							if ( comments_open() || get_comments_number() ) :
								comments_template();
							endif;
	
						endwhile; // End of the loop.
						?>
	
<!-- Partners Logo -->
<div class="partners-logo">
	<?php if( have_rows('partner_logos') ): ?>

		<ul>

		<?php while( have_rows('partner_logos') ): the_row(); 

			// vars
			$image = get_sub_field('image');

			?>

			<li>
				<img src="<?php echo $image; ?>"" />
			</li>

		<?php endwhile; ?>

		</ul>

	<?php endif; ?>
</div>


<!-- end -->

					</main><!-- #main -->
				</div><!-- #primary -->
	
		</div>
		
	</div>
</div>

<?php get_footer(); ?>

