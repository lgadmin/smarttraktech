    <?php
/**
 * The template for displaying all pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package smarttraktech
 */
?>

<?php get_header(); ?>

<?php get_template_part("/inc/featured-image"); ?>

<div class="container pt-lg pb-lg">
	<div class="row">
		
		<div class="col-xs-12">
			
				<div id="primary" class="content-area">
					<main id="main" class="site-main">
						<h1>Media Download</h1>
						<?php
						while ( have_posts() ) : the_post();
						?>
							<ul class="press-download">
				            	<?php
				                    
				            		if( have_rows('press') ):

									 	// loop through the rows of data
									    while ( have_rows('press') ) : the_row();

									        // display a sub field value
									        $title = get_sub_field('title');
									        $image = get_sub_field('image');
									        $pdf = get_sub_field('pdf');

									    ?>

											<li>
												<img src="<?php echo $image; ?>" alt="">
												<div>
													<h2><?php echo $title ?></h2>
													<a class="btn btn-default" href="<?php echo $pdf; ?>">DOWNLOAD</a>
												</div>
											</li>

									    <?php
									    endwhile;
									endif;
								?>
							</ul>
						<?php
						endwhile; // End of the loop.
						?>

					</main><!-- #main -->
				</div><!-- #primary -->
	
		</div>
		
	</div>
</div>

<?php get_footer(); ?>

