<?php get_header(); ?>

<?php get_template_part("/inc/featured-image"); ?>

<div class="container pt-lg pb-lg">
	<div class="row">
		<div class="col-xs-12 col-md-12">
			
				<div id="primary" class="content-area">
					<main id="main" class="site-main technology">
	
						<?php
							$intro_header = get_field('intro-header');
							$intro_content = get_field('intro-content');
						?>
						<div class="intro-block">
							<?php if($intro_header): ?>
								<h3><strong><?php echo $intro_header; ?></strong></h3>
							<?php endif; ?>
							<?php if($intro_content): ?>
								<p><?php echo $intro_content; ?></p>
							<?php endif; ?>
						</div>
						<?php
						// check if the repeater field has rows of data
						if( have_rows('technologies') ):
							?>
							<div class="technology-list">
							<?php
						 	// loop through the rows of data
						    while ( have_rows('technologies') ) : the_row();

						        // display a sub field value
						        $title = get_sub_field('title');
						        $copy = get_sub_field('copy');
						        ?>
									<div class="single-tech">
										<div class="content">
											<?php if($title): ?>
											<h3><?php echo $title; ?></h3>
											<?php endif; ?>
											<div><i class="fa fa-caret-down" aria-hidden="true"></i></div>
											<?php if($copy): ?>
												<p><?php echo $copy; ?></p>
											<?php endif; ?>
										</div>
									</div>
								<?php
						    endwhile;
						    ?>
							</div>
						    <?php
						else :

						    // no rows found

						endif;
						?>
	
					</main><!-- #main -->
				</div><!-- #primary -->
	
		</div>
		
	</div>
</div>

<?php get_footer(); ?>

