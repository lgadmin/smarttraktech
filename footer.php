<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package smarttraktech
 */

?>

	</div><!-- #content -->

		<div class="pt-lg pb-lg footer-wrapper">
			<div class="container-fluid footer ml-lg mr-lg">
				<footer id="colophon" class="site-footer row">

					<div class="col-xs-6 col-lg-2">
						<div>
						<h4>Company</h4>
							<ul>
								<li><a href="/corporate-overview/">Corporate Overview</a></li>
								<li><a href="/executive-management-team/">Management Team</a></li>
								<li><a href="/advisory-board/">Advisory Board</a></li>
								<li><a href="/partners/">Partners</a></li>
								<li><a href="#">Privacy Policy</a></li>
							</ul>
						</div>
					</div>

					<div class="col-xs-6 col-lg-2">
						<div>
						<h4>Products</h4>
						<ul>
							<a href="/seal-trak/"><li>Seal Trak</li></a>
						</ul>

						<h4>Technology</h4>
						<ul class="footer-technology">
							<a href="/secure/"><li>Secure</li></a>
							<a href="/track/"><li>Track</li></a>
							<a href="/notify/"><li>Notify</li></a>
						</ul>
						</div>
					</div>



					<div class="col-sm-6 col-lg-4 footer-address">
						<h4>Corporate Headquarters</h4>
							<address itemscope="" itemtype="http://schema.org/LocalBusiness">
								<span class="card-map-marker" itemprop="address" itemscope="" itemtype="http://schema.org/PostalAddress">
									<span itemprop="streetAddress">Suite 2200 – 75 Fourteenth Street</span><br>
									<span itemprop="addressLocality">Atlanta</span>, <span itemprop="addressRegion">GA</span>&nbsp;<span itemprop="postalCode">30309</span><br>
								</span>

								<span class="card-map-phone" itemprop="telephone">
									<a href="tel:+1778-227-6031">(778) 227-6031</a>
								</span>
							</address>
						<?php echo do_shortcode('[gtranslate]'); ?>
					</div>

					<div class="col-sm-6 col-lg-4">
						
						
						<h4>Stay Connected</h4>
							<!-- <code>STAY CONNECTED</code> -->

								<form class="form-horizontal">
								  <div class="form-group">
								    <div class="col-xs-8 col-sm-8">
								      <input type="email" class="form-control" id="inputEmail3" placeholder="Email">
								    </div>

								  <div class="form-group">
								    <div class="col-xs-2 col-sm-2">
								      <button type="submit" class="btn btn-default">Submit</button>
								    </div>
								  </div>
								</form>

				

				</footer>

					<div class="site-info col-sm-12">
						<div class="row legal">
							<div class="col-sm-6">
								© 2017 Smart Trak Techologies Inc. All Rights Reserved. | <a href="/privacy-policy/">Privacy Policy</a>
							</div>
							<!-- <div class="col-sm-6">
								<ul class="list-inline small pull-right">
									<li><a href="https://www.longevitygraphics.com/" target="_blank">Web Design by Longevity Graphics</li>
								</ul>
							</div> -->

						</div>
					</div>

			</div>
		</div>


</div><!-- #page -->

<a class="scroll-top"><i class="fa fa-angle-double-up" aria-hidden="true"></i></a>

<?php wp_footer(); ?>

</body>
</html>
