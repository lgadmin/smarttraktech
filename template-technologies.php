<?php
/**
 ** Template Name: Technologies Template
 */
?>

<?php get_header(); ?>

<?php get_template_part("/inc/featured-image"); ?>

<div class="container pt-lg pb-lg">
	<div class="row">
		<div class="col-xs-12 col-md-12">
			
				<div id="primary" class="content-area technologies">
					<main id="main" class="site-main">
	
						<?php
							$page_sub_header = get_field('sub-header');
							$page_feature_image = get_field('feature_image');
						?>

						<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
							<?php if($page_sub_header) : ?>
								<h1 class="entry-header">
									<?php echo $page_sub_header ?>
								</h1><!-- .entry-header -->
							<?php endif; ?>
							<div class="entry-content">
								<?php if($page_feature_image): ?>
									<div class="feature-image desktop">
										<img src="<?php echo $page_feature_image; ?>" alt="">
									</div>
								<?php endif; ?>
								<?php echo the_content(); ?>
								<?php if($page_feature_image): ?>
									<div class="feature-image mobile">
										<img src="<?php echo $page_feature_image; ?>" alt="">
									</div>
								<?php endif; ?>
							</div>

							<?php
							$cta = get_field('call_to_action');
							?>
								<?php if($cta['enable']): ?>
									<div class="page_cta">
										<div>
											<?php if($cta['title']): ?>
												<h2><?php echo $cta['title']?></h2>
											<?php endif; ?>
										</div>
										<a href="<?php echo $cta['button_url']; ?>" class="btn-default btn"><?php echo $cta['button_text']; ?></a>
									</div>
								<?php endif; ?>

						</article>
	
					</main><!-- #main -->
				</div><!-- #primary -->
	
		</div>
		
	</div>
</div>

<?php get_footer(); ?>

