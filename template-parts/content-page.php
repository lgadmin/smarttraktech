<?php
/**
 * Template part for displaying page content in page.php
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package smarttraktech
 */

?>
<?php
	$page_sub_header = get_field('sub-header');
?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	<?php if($page_sub_header) : ?>
		<h1 class="entry-header">
			<?php echo $page_sub_header ?>
		</h1><!-- .entry-header -->
	<?php endif; ?>
	<div class="entry-content">
		<?php
			the_content();

			$cta = get_field('call_to_action');
			?>
				<?php if($cta['enable']): ?>
					<div class="page_cta">
						<div>
							<?php if($cta['title']): ?>
								<h2><?php echo $cta['title']?></h2>
							<?php endif; ?>
						</div>
						<a href="<?php echo $cta['button_url']; ?>" class="btn-default btn"><?php echo $cta['button_text']; ?></a>
					</div>
				<?php endif; ?>
			<?php
			wp_link_pages( array(
				'before' => '<div class="page-links">' . esc_html__( 'Pages:', 'smarttraktech' ),
				'after'  => '</div>',
			) );
		?>
	</div><!-- .entry-content -->

	<?php if ( get_edit_post_link() ) : ?>
		<footer class="entry-footer">
			<?php
				edit_post_link(
					sprintf(
						wp_kses(
							/* translators: %s: Name of current post. Only visible to screen readers */
							__( 'Edit <span class="screen-reader-text">%s</span>', 'smarttraktech' ),
							array(
								'span' => array(
									'class' => array(),
								),
							)
						),
						get_the_title()
					),
					'<span class="edit-link">',
					'</span>'
				);
			?>
		</footer><!-- .entry-footer -->
	<?php endif; ?>
</article><!-- #post-<?php the_ID(); ?> -->
